package com.gnioos.authservice.service.mapper;

import com.gnioos.authservice.domain.Authority;
import com.gnioos.authservice.domain.User;
import com.gnioos.authservice.service.dto.GnioosUser;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link User} and its DTO called {@link GnioosUser}.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    public List<GnioosUser> usersToUserDTOs(List<User> users) {
        return users.stream()
            .filter(Objects::nonNull)
            .map(this::userToUserDTO)
            .collect(Collectors.toList());
    }

    public GnioosUser userToUserDTO(User user) {
        return new GnioosUser(user);
    }

    public List<User> userDTOsToUsers(List<GnioosUser> gnioosUsers) {
        return gnioosUsers.stream()
            .filter(Objects::nonNull)
            .map(this::userDTOToUser)
            .collect(Collectors.toList());
    }

    public User userDTOToUser(GnioosUser gnioosUser) {
        if (gnioosUser == null) {
            return null;
        } else {
            User user = new User();
            user.setId(gnioosUser.getId());
            user.setLogin(gnioosUser.getLogin());
            user.setFirstName(gnioosUser.getFirstName());
            user.setLastName(gnioosUser.getLastName());
            user.setEmail(gnioosUser.getEmail());
            user.setTelephone(gnioosUser.getTelephone());
            user.setImageUrl(gnioosUser.getImageUrl());
            user.setActivated(gnioosUser.isActivated());
            user.setLangKey(gnioosUser.getLangKey());
            Set<Authority> authorities = this.authoritiesFromStrings(gnioosUser.getAuthorities());
            user.setAuthorities(authorities);
            return user;
        }
    }

    private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
        Set<Authority> authorities = new HashSet<>();

        if (authoritiesAsString != null) {
            authorities = authoritiesAsString.stream().map(string -> {
                Authority auth = new Authority();
                auth.setName(string);
                return auth;
            }).collect(Collectors.toSet());
        }

        return authorities;
    }

    public User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
