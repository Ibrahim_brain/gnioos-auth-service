package com.gnioos.authservice.web.rest.resource;

import com.gnioos.authservice.service.dto.GnioosUser;
import javax.validation.constraints.Size;

/**
 * View Model extending the GnioosUser, which is meant to be used in the user management UI.
 */
public class ManagedGnioosUserResource extends GnioosUser {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedGnioosUserResource() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ManagedGnioosUserResource{" + super.toString() + "} ";
    }
}
