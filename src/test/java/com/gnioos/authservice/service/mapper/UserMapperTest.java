package com.gnioos.authservice.service.mapper;

import com.gnioos.authservice.domain.User;
import com.gnioos.authservice.service.dto.GnioosUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for {@link UserMapper}.
 */
public class UserMapperTest {

    private static final String DEFAULT_LOGIN = "johndoe";
    private static final Long DEFAULT_ID = 1L;

    private UserMapper userMapper;
    private User user;
    private com.gnioos.authservice.service.dto.GnioosUser GnioosUser;

    @BeforeEach
    public void init() {
        userMapper = new UserMapper();
        user = new User();
        user.setLogin(DEFAULT_LOGIN);
        user.setPassword(RandomStringUtils.random(60));
        user.setActivated(true);
        user.setEmail("johndoe@localhost");
        user.setFirstName("john");
        user.setLastName("doe");
        user.setImageUrl("image_url");
        user.setLangKey("en");

        GnioosUser = new GnioosUser(user);
    }

    @Test
    public void usersToGnioosUsersShouldMapOnlyNonNullUsers() {
        List<User> users = new ArrayList<>();
        users.add(user);
        users.add(null);

        List<GnioosUser> GnioosUserS = userMapper.usersToUserDTOs(users);

        assertThat(GnioosUserS).isNotEmpty();
        assertThat(GnioosUserS).size().isEqualTo(1);
    }

    @Test
    public void GnioosUsersToUsersShouldMapOnlyNonNullUsers() {
        List<GnioosUser> usersDto = new ArrayList<>();
        usersDto.add(GnioosUser);
        usersDto.add(null);

        List<User> users = userMapper.userDTOsToUsers(usersDto);

        assertThat(users).isNotEmpty();
        assertThat(users).size().isEqualTo(1);
    }

    @Test
    public void GnioosUsersToUsersWithAuthoritiesStringShouldMapToUsersWithAuthoritiesDomain() {
        Set<String> authoritiesAsString = new HashSet<>();
        authoritiesAsString.add("ADMIN");
        GnioosUser.setAuthorities(authoritiesAsString);

        List<GnioosUser> usersDto = new ArrayList<>();
        usersDto.add(GnioosUser);

        List<User> users = userMapper.userDTOsToUsers(usersDto);

        assertThat(users).isNotEmpty();
        assertThat(users).size().isEqualTo(1);
        assertThat(users.get(0).getAuthorities()).isNotNull();
        assertThat(users.get(0).getAuthorities()).isNotEmpty();
        assertThat(users.get(0).getAuthorities().iterator().next().getName()).isEqualTo("ADMIN");
    }

    @Test
    public void GnioosUsersToUsersMapWithNullAuthoritiesStringShouldReturnUserWithEmptyAuthorities() {
        GnioosUser.setAuthorities(null);

        List<GnioosUser> usersDto = new ArrayList<>();
        usersDto.add(GnioosUser);

        List<User> users = userMapper.userDTOsToUsers(usersDto);

        assertThat(users).isNotEmpty();
        assertThat(users).size().isEqualTo(1);
        assertThat(users.get(0).getAuthorities()).isNotNull();
        assertThat(users.get(0).getAuthorities()).isEmpty();
    }

    @Test
    public void GnioosUserToUserMapWithAuthoritiesStringShouldReturnUserWithAuthorities() {
        Set<String> authoritiesAsString = new HashSet<>();
        authoritiesAsString.add("ADMIN");
        GnioosUser.setAuthorities(authoritiesAsString);

        User user = userMapper.userDTOToUser(GnioosUser);

        assertThat(user).isNotNull();
        assertThat(user.getAuthorities()).isNotNull();
        assertThat(user.getAuthorities()).isNotEmpty();
        assertThat(user.getAuthorities().iterator().next().getName()).isEqualTo("ADMIN");
    }

    @Test
    public void GnioosUserToUserMapWithNullAuthoritiesStringShouldReturnUserWithEmptyAuthorities() {
        GnioosUser.setAuthorities(null);

        User user = userMapper.userDTOToUser(GnioosUser);

        assertThat(user).isNotNull();
        assertThat(user.getAuthorities()).isNotNull();
        assertThat(user.getAuthorities()).isEmpty();
    }

    @Test
    public void GnioosUserToUserMapWithNullUserShouldReturnNull() {
        assertThat(userMapper.userDTOsToUsers(null)).isNull();
    }

    @Test
    public void testUserFromId() {
        assertThat(userMapper.userFromId(DEFAULT_ID).getId()).isEqualTo(DEFAULT_ID);
        assertThat(userMapper.userFromId(null)).isNull();
    }
}
